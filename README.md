# Geekstuff.dev / Devcontainers / Features / AWS cli

This devcontainer feature installs AWS cli and its autocomplete.

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/basics": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/aws-cli": {}
    }
}
```

You can use a `debian` or `ubuntu` image as the base.

Alpine was attempted but was also quite tedious.

This feature requires the basics feature which adds a `dev` non-root user
and here we mount an isolated and dedicated docker volume for the `~/.aws` folder.

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/aws-cli/-/tags).
